Tic Tac Toe Game

This project will be about simulating a tic tac toe game in Lisp.There are functions to show the board
and to add pieces to the board. Mainly the play(), putx(), and puto() functions are used to play the
actual game, any other functions are used to run the game itself and should not be invoked by a player.
There is also a dumb AI that can be invoked to play against, it uses randomness, but will actively try 
to fill three spots and win. So its psuedo random how it plays the game. Player can either start or the
AI can start in the game.
----------------------
Instructions:
Install: 
	1. Take the TicTacToe.lisp files and find a place to put it.
	2. Open a Lisp REPL
	3. Run the below code:
		
		(load "YOUR/PATH/TO/THE/FILE/TicTacToe.lisp")
	
	4. Hit enter and a T should appear below the input, this means the file was succesfuly loaded.
	5. If error then check the file path or see if the code version is stable.
	
NOTE: This game has a play and standby state. A user must run the play function before beggining a game.
To play a game:
	1. Use install intructions to load up the game file.
	2. Run the folling command to begin a game.
		
		(play)
	
	3. A board will appaear and a list of avaialbale spaces will be given.
	NOTE: X always starts in this version of the game, so dont try to use puto first.
	4. Depending on whoes turn it is the player can run one of the two codes below:
		
		For the X player.
		(putx SPACE-YOU-WISH-TO-PUT-AN-X-IN)
		EX. (putx 1)
	
		For the O player.
		(puto SPACE-YOU-WISH-TO-PUT-AN-O-IN)
		EX. (puto 2)
	5. Keep changing turns until either a winner is chosen or its a tie.
	6. Use the play function to retstart the game if you wish to play again.
	
To play a gain versus AI:
	1. Use install intructions to load up the game file.
	2. Run the folling command to begin a game.
		
		For when the player wants to start first.
		(play-ai 0) 
		
		For when the player wants the AI to start first.
		(play-ai 1) 
	
	3. A board will appaear and a list of avaialbale spaces will be given.
	4. Follow steps 4-5 of the regular game.
	5. Use commands in step 2 to play again versus an AI.
----------------------
Built With:
Lisp - Main Code

----------------------
Author(s):
Nghia Huynh

----------------------
License:
This project is license under nobody and is free to use for all.

----------------------
Acknowledgements:
Google - Telling me what to do.
;;TicTacToe.lisp
;;Nghia Huynh
;;Compilers 1

;;This global is for the tictactoe board.
(defparameter *board* (list '- '- '- '- '- '- '- '- '-))
;;Indexing table to help with calls.
(defparameter *index* (list '0 '1 '2 '3 '4 '5 '6 '7 '8))
;;This variable keeps the number of available empty spots for pieces.
(defparameter *available* (list '0 '1 '2 '3 '4 '5 '6 '7 '8))
;;This checks for the gamestate of the tictactoe game.
;;If state 0 then game is in progress
;;If state 1 then game has not been started/has already finished.
;;If state 2 then game is being played against an AI.
;;Need player to run play function to set state to 0 or play-ai to set state 2.
(defparameter *game-state* 1)

;;==========Functions below are for running the game!==========
;;==========DO NOT CALL IF YOU ARE JUST PLAYING THE GAME AS A REGULAR USER!==========

;;Displays the current board and informs the user what spaces are available.

;;Cuts an ele from the beggining of a list
(defun cut-ele(input ele)
	(if (eql ele (car input)) (cdr input) input)
)
;;Recursively removes the ele from a list
(defun remove-ele(input ele)
		;; Checks to seee if we are at the end of the list
		(if (null (cdr input)) 
		;; This is for when the last element of the list something; if it is return nil.
		(if (eql ele (car input)) nil input)
		;; Cons the first element of a list to the rest of list after 
		;; removing the zero from the front of the split.
		(cons (car (cut-ele input ele)) (remove-ele (cdr (cut-ele input ele)) ele)))
)

(defun display (board)
	(format t "=================")
	(do ( (i 0 (+ i 1)))
		( (= i 9) 'done)
		(if ( = (mod i 3) 0)  (format t "~%||") nil)
		(format t " ~A ||" (nth i board))
		;;Checks to see if the space is empty or not, removes the taken up spaces from available variable.
		(if (equal (nth i board) '-) nil (setf *available* (remove-ele *available* i)))
	)
	(format t "~%=================~%")
	;;Prints the available spots for the user.
	(format t "Available Spaces~%")
	;;If non are avialable it informs the user.
	(if (null *available*)
	(format t "No spots left!~%")
	(format t "~{~A~^, ~}~%" *available*)
	)
)

;;Checks if three elements of a list are equal
(defun threequal (input)
	(and (equal (first input) (second input)) (equal (second input) (third input)))
)
;;Checks if a list of 3 elements has an empty value, return nil if none are present.
(defun check-empty (input)
	(if (equal (first input) '-) T 
		(if (equal (second input) '-) T
			(if (equal (third input) '-) T nil)))
)
;;Gets a row of the board given the index of the row form 0 to 2
(defun get-row (input row)
    (list (nth (* 3 row) input) (nth (+ (* 3 row) 1) input) (nth (+ (* 3 row) 2) input))
)
;;Gets a column of the board given the index of the colums form 0 to 2
(defun get-col (input col)
  (list (nth col input) (nth (+ col 3) input) (nth (+ col 6) input))
)
;;Gets the forward diagonal of the board
(defun get-forward-diag (input)
	(list (nth 2 input) (nth 4 input) (nth 6 input))
)
;;Gets the backwards diagonal of the board
(defun get-backward-diag (input)
	(list (nth 0 input) (nth 4 input) (nth 8 input))
)
;;Takes in a 3 elememt list and checks to see if it was O or X that won.
(defun winner (input)
	;;Inform user game is over.
	(format t "GAME OVER!~%")
	;;Check if a tie has been passed in.
	(if (equal input nil)
		(format t "It's a tie!")
		;;Check if the first element of the winning list is x
		;;If so then X wins else O wins.
		(if (equal (first input) 'x) 
			(format t "X has won the game!~%")
			(format t "O has won the game!~%")
		)
	)
	;;Sets game state to 1 because game has concluded.
	(setf *game-state* 1)
)

;; Checks the board to see if their is a winning row.
(defun victory (input)
	;;Checks if three elements are equal and that non of the three elements are empty.
	;;If both conditions are true call function to check for who won the game.
	;;This is done in nested if statments to check for the 8 different patterns for 3 elememts
	;;Checks victories in all rows.
	(if (and (threequal (get-row input 0)) (not (check-empty (get-row input 0)))) (winner (get-row input 0))
	(if (and (threequal (get-row input 1)) (not (check-empty (get-row input 1)))) (winner (get-row input 1))
	(if (and (threequal (get-row input 2)) (not (check-empty (get-row input 2)))) (winner (get-row input 2))
	;;Check victories in all columns
	(if (and (threequal (get-col input 0)) (not (check-empty (get-col input 0)))) (winner (get-col input 0))
	(if (and (threequal (get-col input 1)) (not (check-empty (get-col input 1)))) (winner (get-col input 1))
	(if (and (threequal (get-col input 2)) (not (check-empty (get-col input 2)))) (winner (get-col input 2))
	;;Checks for victories in forward and backward diagonals.
	(if (and (threequal (get-forward-diag input)) (not (check-empty (get-forward-diag input)))) (winner (get-forward-diag input))
	(if (and (threequal (get-backward-diag input)) (not (check-empty (get-backward-diag input)))) (winner (get-backward-diag input))
	;;Checks if the board is full after all victory checks.
	;;If true then the game has tied.
	(if (equal *available* nil) (winner nil) nil
	;;I left this cause it was pretty, but a nightmare of if statements.
									)
								)
							)	
						)
					)
				)
			)
		)
	)
)
;;Puts a chosen piece on a spot on the board.
(defun put-move (position piece)
	(setf (nth position *board*) piece)
	(display *board*)
	(victory *board*)
	;;This checks for if the player is playing against an ai or not.
	(if (equal *game-state* 2)
		;;Checks to see what piece the player is playing and will tell the ai to play the other one.
		(if (equal piece 'x)
			(put-ai *board* 'o)
			(put-ai *board* 'x)
		)
	)
)

;;==========Functions below are for the AI when it plays!==========
;;==========DO NOT CALL IF YOU ARE NOT A ROBOT!==========
;;This function is for starting an AI game and setting the game state for AI play.
(defun play-ai (start)
	;;Set globals for beggining a game.
	(setf *board* (list '- '- '- '- '- '- '- '- '-))
	(setf *available* (list '0 '1 '2 '3 '4 '5 '6 '7 '8))
	;;Game state 2 is for AI play.
	(setf *game-state* 2)
	;;Inform user that game has started.
	(format t "Tic Tac Toe game versus AI has begun! Beep! Boop!~%")
	;;If the player inputs 1 for start that means the AI will move first.
	(if (= start 1)
		(put-ai *board* 'x)
		(display *board*)
	)
)
;;Checks if a 3 ele list contains two empty spots and one filled spot.
(defun check-one (input ele)
	(if (and (equal (first input) ele) (equal (second input) '-) (equal (third input) '-)) T
	(if (and (equal (first input) '-) (equal (second input) ele) (equal (third input) '-)) T
	(if (and (equal (first input) '-) (equal (second input) '-) (equal (third input) ele)) T nil)))
)
;;Checks if a 3 ele list contains one empty spot and two filled spots of the same elements.
(defun check-two (input ele)
	(if (and (equal (first input) '-) (equal (second input) ele) (equal (third input) ele)) T
	(if (and (equal (first input) ele) (equal (second input) '-) (equal (third input) ele)) T
	(if (and (equal (first input) ele) (equal (second input) ele) (equal (third input) '-)) T nil)))
)
;;Gets all lists of 3 that have atleast one of the ele in them and puts them into a list.
(defun gather-check-one (board ele)
	;;This intersection is used to find the available spots that the ai will play based on the data
	;;from the currently played pieces.
	(intersection *available* (append 
			(if (check-one (get-row board 0) ele) (get-row *index* 0))
			(if (check-one (get-row board 1) ele) (get-row *index* 1))
			(if (check-one (get-row board 2) ele) (get-row *index* 2))
			(if (check-one (get-col board 0) ele) (get-col *index* 0)) 
			(if (check-one (get-col board 1) ele) (get-col *index* 1)) 
			(if (check-one (get-col board 2) ele) (get-col *index* 2))
			(if (check-one (get-forward-diag board) ele) (get-forward-diag *index*))
			(if (check-one (get-backward-diag board) ele) (get-backward-diag *index*))
	))
)
;;Gets all list of 3 that have atleast two of the same ele in them and puts them into a list.
(defun gather-check-two (board ele)
	;;This intersection is used to find the available spots that the ai will play based on the data
	;;from the currently played pieces.
	(intersection *available* (append 
			(if (check-two (get-row board 0) ele) (get-row *index* 0))
			(if (check-two (get-row board 1) ele) (get-row *index* 1))
			(if (check-two (get-row board 2) ele) (get-row *index* 2))
			(if (check-two (get-col board 0) ele) (get-col *index* 0)) 
			(if (check-two (get-col board 1) ele) (get-col *index* 1)) 
			(if (check-two (get-col board 2) ele) (get-col *index* 2))
			(if (check-two (get-forward-diag board) ele) (get-forward-diag *index*))
			(if (check-two (get-backward-diag board) ele) (get-backward-diag *index*))
	))
)
;;This places a piece for the AI depending on the turn and then it displays the board after it moves.
(defun put-turn (position)
	;;Checks for whose turn it is odd is o's and even is x's
	(if (eql (mod (list-length *available*) 2) 0)
		(setf (nth position *board*) 'o)
		(setf (nth position *board*) 'x)
	)
	(display *board*)
	(victory *board*)
)
;;Taks in a board and either and x or an o and calculates the potential play spots using above functions.
(defun put-ai(board piece)
	(if (and (null (gather-check-one board piece)) (null (gather-check-two board piece)))
		(put-turn (nth (random (list-length *available*)) *available*))
		(if (null (gather-check-two board piece))
			(put-turn (nth (random (list-length (gather-check-one board piece))) (gather-check-one board piece)))
			(put-turn (nth (random (list-length (gather-check-two board piece))) (gather-check-two board piece)))
		)
	)
)

;;==========Functions below are for playing the game!==========
;; Resets game start so that game can be started anew.
(defun play ()
	;;Set globals for beggining a game.
	(setf *board* (list '- '- '- '- '- '- '- '- '-))
	(setf *available* (list '0 '1 '2 '3 '4 '5 '6 '7 '8))
	(setf *game-state* 0)
	;;Inform user that game has started.
	(format t "Tic Tac Toe game has begun!~%")
	(display *board*)
)

;;Places an X on the board
(defun putx (position)
	;;Checks to see if a game is in progress or not.
	(if (equal *game-state* 1)
	(format t "Please run play function to start/reset the game!~%")
	;;Using turn number to see if its X's turn.
	(if (eql (mod (list-length *available*) 2) 0)
		(format t "Its not X's turn! Try again!~%")
		;;Checks if the spaces is alread occupied
		(if (equal (nth position *board*) '-)
			(put-move position 'x)
			(format t "This space is taken! Try again!~%")
		)
	)
	)
)
;;Places an O on the board
(defun puto (position)
	(if (equal *game-state* 1)
	(format t "Please run play function to start/reset the game!~%")
	;;Using turn number to see if its O's turn.
	(if (eql (mod (list-length *available*) 2) 1)
		(format t "Its not O's turn! Try again!~%")
		;;Checks if the spaces is alread occupied
		(if (equal (nth position *board*) '-)
			(put-move position 'o)
			(format t "This space is taken! Try again!~%")
		)
	)
	)
)